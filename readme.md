# Mazes for Programmers, Illustrated with Clerk
I'm working through _Mazes for Programmers_ by Jamis Buck. I'll use the Clerk
library to produce notebooks demonstrating the maze algorithms.

## Run the examples
Evaluate the `mazes.user` namespace in a repl, then visit http://localhost:7777/ 
