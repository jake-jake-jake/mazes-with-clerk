(ns mazes.graphics.constants)

(def default-path-fill "#EF5B5B")

;; Math constants
(def sqrt3 (Math/sqrt 3))
