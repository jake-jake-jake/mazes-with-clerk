(ns mazes.graphics.load
  (:require [mazes.graphics.polar]
            [mazes.graphics.grid]
            [mazes.graphics.sigma]))
