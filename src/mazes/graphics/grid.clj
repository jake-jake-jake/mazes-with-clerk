(ns mazes.graphics.grid
  (:require [mazes.graphics.multi :as multi]
            [mazes.graphics.constants :as constants]))

(defmethod multi/render-cell :grid
  [{:keys [maze/cell-size]}
   {:keys [coord x y :cell/north-wall? :cell/east-wall?]}]
  (let [[x y] (or coord [x y])
        x1 (* x cell-size)
        x2 (+ x1 cell-size)
        y1 (* y cell-size)
        y2 (+ y1 cell-size)]
    [:svg

     (when north-wall?
       [:line {:x1 x1 :x2 x2 :y1 y2 :y2 y2 :stroke "black" :stroke-width 3 :stroke-linecap "round"}])
     (when east-wall?
       [:line {:x1 x2 :x2 x2 :y1 y1 :y2 y2 :stroke "black" :stroke-width 3 :stroke-linecap "round"}])]))

(defmethod multi/render-base :grid
  [{:keys [maze/cell-size maze/height maze/width maze/xform]} & contents]
  (let [pix-h (* cell-size height)
        pix-w (* cell-size width)
        xform (or xform
                  (format  "scale(1,-1) translate(0, %s)" (- pix-h)))]
    [:svg {:width pix-w :height pix-h :xmlns "http://www.w3.org/2000/svg"}
     [:g {:transform xform}
      contents
      [:rect {:width pix-w :height pix-h :stroke "black" :fill "none" :stroke-width 7}]]]))

(defmethod multi/flood-cell :grid
  [{:keys [maze/cell-size
           maze/longest-path
           maze/path-fill]
    :or   {path-fill constants/default-path-fill}} [x y] n]
  (let [px (* x cell-size)
        py (* y cell-size)
        fill-opacity (if longest-path
                       (/ n longest-path)
                       1)]
    [:svg {:width cell-size :height cell-size :x px :y py}
     [:rect {:width "100%" :height "100%" :fill path-fill :z "-1" :fill-opacity fill-opacity :stroke "none"}]]))
