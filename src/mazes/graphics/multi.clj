(ns mazes.graphics.multi)

(defn dispatch [opts & _]
  (:maze/type opts))

(defmulti render-cell dispatch)

(defmulti render-base dispatch)

(defmulti flood-cell dispatch)
