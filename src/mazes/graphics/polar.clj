(ns mazes.graphics.polar
  (:require [mazes.graphics.multi :as multi]
            [clojure.math :as math]
            [clojure.string :as str]))

(defn- polar->x-y [r theta]
  (let [x (-> theta math/to-radians math/cos (* r))
        y (-> theta math/to-radians math/sin (* r))]
    [x y]))

(defn move->x-y [[x y]]
  (format "M %.2f,%.2f" x y))

(defn line->x-y [[x y]]
  (format "L %.2f,%.2f" x y))

(defn radius-arc->x-y [r [x y]]
  (format "A %s %s 0 0 1 %.2f,%.2f" r r x y))

(defn- make-arc [r theta closed?]
  (let [[x y] (polar->x-y r theta)]
    (comment)
    (if closed?
      (radius-arc->x-y r [x y])
      (move->x-y [x y]))))

(defn build-arc-sections [r children closed-children cw]
  (->> (conj children (or cw [1 360]))
       (partition 2 1)
       (mapv (fn [[child [_ end-theta :as nchild]]]
               (make-arc r
                         end-theta
                         (contains? closed-children child))))))

;;; TODO this is pretty fugly
(defmethod multi/render-cell :polar
  [{:keys [:maze/cell-size] :as opts}
   {:keys [coord cw cw-closed? children closed-children]}]
  (let [[ring rot] coord
        inner (* cell-size ring)
        outer (* cell-size (inc ring))
        start-point (-> (polar->x-y outer rot) move->x-y)
        end-point (when (and (seq cw) cw-closed?)
                    (let [cw-rot (second cw)
                          coord (polar->x-y inner cw-rot)]
                      (line->x-y coord)))
        arc-sections (if (seq children)
                       (build-arc-sections outer children closed-children cw)
                       [(radius-arc->x-y outer (polar->x-y outer (second cw)))])
        d (->> (concat [start-point] arc-sections [end-point])
               (remove nil?)
               (str/join "\n"))]
    [:path {:d d :fill "none"}]))

(defmethod multi/render-base :polar
  [{:keys [maze/cell-size maze/height maze/xform]} & contents]
  (let [radius (* cell-size height)
        diam   (* 2 (inc radius))
        viewbox (format "%s %s %s %s"
                        0
                        0
                        diam
                        diam)
        xform (or xform
                  (format  "translate(%s, %s)" (inc radius) (inc radius)))]
    [:svg {:width diam :height diam :viewBox viewbox :xmlns "http://www.w3.org/2000/svg"}
     [:g {:stroke "black" :stroke-width 2 :stroke-linecap "round" :transform xform}
      contents]]))

(comment
  (def center-cell {:coord [0 0],
                    :ccw nil,
                    :cw nil,
                    :children [[1 0] [1 120] [1 240]],
                    :parent nil,
                    :closed-children #{[1 0] [1 240] [1 120]},
                    :cw-closed? true})
  (def outer-cell {:coord [1 240],
                   :ccw [1 120],
                   :cw [1 0],
                   :children nil,
                   :parent [0 0],
                   :closed-children #{},
                   :cw-closed? true})
  (def opts {:maze/type :polar
             :maze/cell-size 10
             :maze/height 3
             :maze/width 4})

  (multi/render-cell opts center-cell)
;; =>
;
  )
