(ns mazes.graphics.sigma
  (:require [mazes.graphics.multi :as multi]
            [mazes.algorithms.sigma :as sigma]
            [mazes.graphics.constants :as const]
            [clojure.string :as str]))

(defn circle-at [[x y]]
  [:circle {:cx x
            :cy y
            :r "1px"
            :stroke "black"
            :fill "black"}])

(defn cell-path-piece [opts cx])

(defn move->x-y [[x y]]
  (format "M %.2f,%.2f" x y))

(defn line->x-y [[x y]]
  (format "L %.2f,%.2f" x y))

;; cell-size here is length in pixels from point to opposite point of hexagon
(defn axial->cx-cy [{:keys [sigma/pointy-top? maze/cell-size]
                     :or {pointy-top? true}}
                    [q r]]
  (let [f1 (fn [c1] (* cell-size 1.5 c1))
        f2 (fn [c1 c2]
             (* cell-size
                (+ (* const/sqrt3 c2)
                   (* 0.5 const/sqrt3 c1))))]
    (if pointy-top?
      [(f2 r q) (f1 r)]
      [(f2 q r) (f1 q)])))


(defn hex-vertices
  "Produce x,y coordinates of vertices, proceeding from 2-oclock clockwise."
  [size [cx cy]]
  (let [l (float size)
        alt (* const/sqrt3 (/ l 2))]
    [[(+ cx alt) (- cy (/ l 2))]
     [(+ cx alt) (+ cy (/ l 2))]
     [cx         (+ cy l)]
     [(- cx alt) (+ cy (/ l 2))]
     [(- cx alt) (- cy (/ l 2))]
     [cx         (- cy l)]]))

(defmethod multi/render-cell :sigma
  [{:keys [maze/cell-size] :as opts}
   {:keys [coord] :as cell}]
  (let [center (axial->cx-cy opts coord)
        vertices (hex-vertices cell-size center)
        walls (->> (map #(false? (get cell %)) sigma/neighbor-coords)
                   (map vector vertices))
        step-1 (move->x-y (first vertices))
        others (->> walls
                    (drop 1)
                    (map (fn [[c walled?]]
                           (if walled?
                             (line->x-y c)
                             (move->x-y c)))))
        ;; TODO this is p. bad.
        last-1 (when (false? (get cell [1 -1])) [(line->x-y (first vertices))])
        d (str/join "\n" (concat [step-1] others last-1))]
    [:path {:d d :fill "none"}]))

(defmethod multi/render-base :sigma
  [{:keys [maze/cell-size maze/height maze/width]} & contents]
  (let [s (+ 5 (float cell-size))
        r-shift (inc (* const/sqrt3 s))
        v-shift (* s 1.5)]
    [:svg {:width (* const/sqrt3 cell-size (inc width))
           :height (* cell-size 1.5 (inc height))
           :xmlns "http://www.w3.org/2000/svg"}
     [:g {:stroke "black"
          :stroke-width 3
          :stroke-linecap "round"
          :transform (format "translate(%.2f, %.2f)" s s)}
      contents]]))

(comment
  (hex-vertices 10 [0 0])
  (def cell {:coord [0 0],
             [1 0] false,
             [0 1] false,
             [-1 1] false,
             [-1 0] false,
             [0 -1] false,
             [1 -1] false})
  ;
  )
