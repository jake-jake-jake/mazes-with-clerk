(ns mazes.graphics
  (:require
   [mazes.graphics.multi :as multi]
   [mazes.graphics.load]
   [mazes.graphics.constants :as const]))

(def default-path-fill "#EF5B5B")
(defn circle-label [[x y] n {:keys [maze/cell-size
                                    maze/longest-path
                                    maze/path-fill]
                             :or   {path-fill const/default-path-fill}}]
  (let [px (* x cell-size)
        py (* y cell-size)
        r (* (/ cell-size 2) 0.7)
        fill-opacity (if longest-path
                       (/ n longest-path)
                       1)]
    [:svg {:width cell-size :height cell-size :x px :y py}
     [:circle {:cx "50%"
               :cy "50%"
               :r r
               :stroke "black"
               :stroke-width "1.5"
               :fill path-fill
               :fill-opacity fill-opacity}]
     [:text {:x "50%"
             :y "50%"
             ;; :transform (format "scale(1, -1) translate(0, %s)" (- py))
             :text-anchor "middle"
             :dy ".3em"
             :font-size "16"
             :fill "black"} n]]))

;; first pass of graphics functions, imbricates generation logic with rendering logic
(defn maze-base [{:keys [cell-size height width] :as opts} & contents]
  (let [pix-h (* cell-size height)
        pix-w (* cell-size width)]
    [:svg {:width pix-w :height pix-h :xmlns "http://www.w3.org/2000/svg"}
     [:rect {:width pix-w :height pix-h :stroke "black" :fill "white" :stroke-width 7}]
     contents]))

(defn grid-cell
  [{:keys [x
           y
           cell-size
           top?
           right?]}]
  (let [x1 (or x 0)
        x2 (+ x1 cell-size)
        y1 (or y 0)
        y2 (+ y1 cell-size)]
    [:svg
     (when top?
       [:line {:x1 x1 :x2 x2 :y1 y1 :y2 y1 :stroke "black" :stroke-width 3 :stroke-linecap "round"}])
     (when right?
       [:line {:x1 x2 :x2 x2 :y1 y1 :y2 y2 :stroke "black" :stroke-width 3 :stroke-linecap "round"}])]))

(defn cells-by-row [{:keys [cell-size
                            width
                            height]}]
  (for [y (reverse (range 0
                          (inc (* cell-size height))
                          cell-size))
        x (range 0 (* cell-size width) cell-size)]
    [x y]))

;; second pass of graphics functions
(defn render-maze [opts cells & extra]
  (multi/render-base opts
                     extra
                     (map #(multi/render-cell opts %) cells)))
