(ns mazes.util
  (:require [clojure.data.generators :as gen]
            [clojure.string :as str]
            [hiccup2.core :as hic]))

(defn symbol->link [s]
  [:a {:href (str  "./notebooks/" s "/index.html")} (str/capitalize s)])

(defn hiccup->html [v]
  (hic/html v))

(defmacro with-seeded-random [seed & body]
  `(binding [gen/*rnd* (java.util.Random. ~seed)]
     ~@body))

(defn srnd-int [n]
  (gen/uniform 0 n))

(defn srnd-nth [coll]
  (if (< 1 (count coll))
    (gen/rand-nth coll)
    (first coll)))

(defn chalk->seq [c]
  (cycle (map-indexed (fn [i j] [i j]) c)))

(defn solve [chalk k]
  (reduce (fn [acc [i n]] (let [c (- acc n)]
                            (if (>= 0 c)
                              (reduced i)
                              c)))

          k
          (chalk->seq chalk)))
