(ns mazes.algorithms.grid
  (:require [mazes.algorithms.multi :as multi]
            [mazes.util :as util]))

(defn cells-by-row [{:keys [maze/width
                            maze/height]}]
  (for [y (range 0 height)
        x (range 0 width)]
    [x y]))

(defn cells-by-col [opts]
  (->> (cells-by-row opts)
       (sort-by (juxt first second))))

(defmethod multi/neighbors :grid [_ctx [x y]]
  [[(dec x) y] [(inc x) y]
   [x (dec y)] [x (inc y)]])

(defmethod multi/accessible-neighbors :grid [ctx coord cell-map]
  (when-let [c (get cell-map coord)]
    (let [[w e s n] (multi/neighbors ctx coord)]
      (cond-> []
        (and (not (:cell/north-wall? c))
             (get cell-map n))
        (conj n)

        (and (not (:cell/east-wall? c))
             (get cell-map e))
        (conj e)

        (when-let [s-c (get cell-map s)]
          (not (:cell/north-wall? s-c)))
        (conj s)

        (when-let [w-c (get cell-map w)]
          (not (:cell/east-wall? w-c)))
        (conj w)))))

(defmethod multi/maze-cells :grid [ctx]
  (cells-by-row ctx))

(defmethod multi/closed-maze :grid [ctx]
  (->> (multi/maze-cells ctx)
       (map (fn [coord] [coord {:coord coord :cell/north-wall? true :cell/east-wall? true}]))
       (into {})))

(defmethod multi/start-coord :grid
  ; TODO this will not work with masks... instead just choose a random key from
  ; the cell-map. Preserving current functionality for maintaining test
  ; determinism during refactor.
  [{:keys [maze/width maze/height]} _cell-map]
  [(util/srnd-int width) (util/srnd-int height)])

(defmethod multi/link-cells :grid [_ctx start end cell-map]
  (let [start-cell (get cell-map start)
        end-cell (get cell-map end)
        [x-move y-move] (mapv - end start)
        new-start-cell (cond-> start-cell
                         (pos? y-move) (assoc :cell/north-wall? false)
                         (pos? x-move) (assoc :cell/east-wall? false))
        new-end-cell (cond-> end-cell
                       (neg? y-move) (assoc :cell/north-wall? false)
                       (neg? x-move) (assoc :cell/east-wall? false))]
    (assoc cell-map
           start new-start-cell
           end   new-end-cell)))
