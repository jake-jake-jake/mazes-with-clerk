(ns mazes.algorithms.polar
  (:require [mazes.algorithms.multi :as multi]
            [mazes.util :as util]))

(def pi-over-180 (/ Math/PI 180))

(defn arclen [r theta]
  (-> pi-over-180
      (* theta)
      (* r)))

(def default-opts {:maze/type :polar
                   :maze/cell-size 10
                   :maze/height 4
                   :maze/width 4})

(def default-ctx {:maze/ctx {::ring-parameters {0 1
                                                1 3}}})

(defn ring-sections [{:keys [:maze/cell-size]} ring]
  (if (zero? ring)
    1
    (let [r (* cell-size ring)]
      (reduce (fn [_ sections]
                (when (<= (arclen r (/ 360 sections))
                          (* 3 cell-size))
                  (reduced sections)))
              1
              (iterate #(* 2 %) 3)))))

(defn ring-parameters* [{:keys [:maze/height] :as opts}]
  (mapv (fn [r] [r (ring-sections opts r)]) (range height)))

(def ring-parameters (memoize ring-parameters*))

(defn ring-arc-starts [num-arcs]
  (let [step (/ 360 num-arcs)]
    (range 0 360 step)))

(defn prev-and-next-rot [rot num-arcs]
  (if (> num-arcs 1)
    (let [step (/ 360 num-arcs)
          p    (- rot step)
          n    (+ rot step)]
      (mapv #(mod % 360) [p n]))
    [0 360]))

(defmethod multi/accessible-neighbors :polar
  [_opts coord cell-map]
  (when-let [cell (get cell-map coord)]
    (let [{:keys [ccw cw cw-closed? parent children closed-children]} cell
          ccw-closed? (some->> (get cell-map ccw)
                               :cw-closed?)
          parent-closed? (some-> (get cell-map parent)
                                 :closed-children
                                 (contains? coord))
          open-children (reduce #(disj %1 %2) (set children) closed-children)]
      (cond-> []
        (and (seq ccw)
             (not ccw-closed?))    (conj ccw)
        (not cw-closed?)           (conj cw)
        (and (seq parent)
             (not parent-closed?)) (conj parent)
        (seq open-children)        (concat open-children)))))

(defmethod multi/link-cells :polar
  [_opts start end cell-map]
  ;; TODO this sort-by is fugly.
  (let [[c1 c2] (sort-by (juxt first (fn [x]
                                       (if (zero? (second x))
                                         360
                                         (second x)))) [start end])
        are-siblings? (= (first c1) (first c2))]
    (if are-siblings?
      (assoc-in cell-map [c1 :cw-closed?] false)
      (update-in cell-map [c1 :closed-children] #(disj % c2)))))

(defmethod multi/maze-cells :polar [opts]
  (->> opts
       ring-parameters
       (mapcat (fn [[r arcs]] (for [s (ring-arc-starts arcs)] [r s])))))

(defn cell-data [opts [ring rot]]
  (let [ring-params (into {} (ring-parameters opts))
        [ccw cw]   (prev-and-next-rot rot (get ring-params ring))
        [p n] (->> (prev-and-next-rot rot (get ring-params ring))
                   (remove #(= rot (mod % 360)))
                   (mapv (fn [r] [ring r])))
        children    (when-let [step (get ring-params (inc ring))]
                      (->> (/ 360 step)
                           (range rot (if (zero? cw)
                                        360
                                        cw))
                           (mapv (fn [r] [(inc ring) r]))))
        parent      (when-let [step (get ring-params (dec ring))]
                      (->> (/ 360 step)
                           (range 0 360)
                           (filter #(>= rot %))
                           last
                           (conj [(dec ring)])))]
    {:coord    [ring rot]
     :ccw      p
     :cw       n
     :children children
     :parent   parent}))

(defmethod multi/neighbors :polar
  [opts [ring rot]]
  (let [{:keys [ccw cw children parent]} (cell-data opts [ring rot])]
    (cond-> children
      (seq ccw) (conj ccw)
      (seq cw) (conj cw)
      (seq parent) (conj parent))))

(defmethod multi/closed-maze :polar [opts]
  (->> opts
       multi/maze-cells
       (map (fn [coord] [coord (cell-data opts coord)]))
       (map (fn [[c n]]
              [c (assoc n
                        :closed-children (into #{} (:children n))
                        :cw-closed?    true)]))
       (into {})))

(defmethod multi/start-coord :polar
  [_opts cell-map]
  [0 0])

(comment
  (def m (multi/closed-maze default-opts))
  (cell-data default-opts [2 300])
  (multi/neighbors default-opts [3 330])
  (multi/accessible-neighbors default-opts [2 300] m)
                                        ;
  )
