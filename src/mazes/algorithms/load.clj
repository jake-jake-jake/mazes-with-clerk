(ns mazes.algorithms.load
  (:require [mazes.algorithms.grid]
            [mazes.algorithms.polar]
            [mazes.algorithms.sigma]))
