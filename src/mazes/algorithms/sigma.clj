(ns mazes.algorithms.sigma
  (:require [mazes.algorithms.multi :as multi]
            [mazes.util :as util]))

(def default-opts {:maze/type :sigma
                   :maze/cell-size 10
                   :maze/height 3
                   :maze/width 3})

(def neighbor-coords
  [[1 -1] [1  0] [0  1]
   [-1 1] [-1 0] [0 -1]])

(defmethod multi/neighbors :sigma
  [_ coord]
  (mapv #(mapv + coord %) neighbor-coords))

(defmethod multi/maze-cells :sigma
  [{:keys [maze/width maze/height]}]
  (mapcat identity (for [r (range height)
                         :let [n (quot r 2)
                               qs (take width (range (- n) (+ (- n) width)))]]
                     (map (fn [q] [q r]) qs))))

(defmethod multi/start-coord :sigma
  [_opts cell-map]
  (util/srnd-nth (keys cell-map)))

(defmethod multi/closed-maze :sigma
  [opts]
  (->> (multi/maze-cells opts)
       (map (fn [coord] [coord
                         {:coord coord
                          [1 0]  false
                          [0 1]  false
                          [-1 1] false
                          [-1 0] false
                          [0 -1] false
                          [1 -1] false}]))

       (into {})))

(defmethod multi/accessible-neighbors :sigma
  [_opts coord cell-map]
  (when-let [c (get cell-map coord)]
    (let [coords (->> (filter #(get c %) neighbor-coords)
                      (map (fn [x]
                             (let [n (mapv + x coord)]
                               [n (and (get c x)
                                       (contains? cell-map n))])))
                      (filter second)
                      (map first))]
      (->> (map (fn [n]
                  (let [step (mapv - coord n)]
                    [n (get-in cell-map [n step])])) coords)
           (filter #(true? (second %)))
           (map first)))))

(defmethod multi/link-cells :sigma
  [_opts c1 c2 cell-map]
  (let [s1 (mapv - c2 c1)
        s2 (mapv - c1 c2)]
    (-> cell-map
        (assoc-in [c1 s1] true)
        (assoc-in [c2 s2] true))))

(comment
  (multi/neighbors default-opts [3 4])
  (def closed (multi/closed-maze default-opts))
  (multi/accessible-neighbors default-opts [0 0] closed)
  (def linked (multi/link-cells default-opts [1 1] [0 1] closed))
  (multi/accessible-neighbors default-opts [0 1] linked))
