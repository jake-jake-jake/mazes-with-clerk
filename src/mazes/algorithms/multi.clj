(ns mazes.algorithms.multi)

(defn dispatch [opts]
  (:maze/type opts))

(defmulti neighbors
  (fn [opts _coord] (:maze/type opts)))

(defmulti accessible-neighbors
  (fn [opts _coord _cell-map] (:maze/type opts)))

(defmulti link-cells
  (fn [opts _start _end _cell-map]
    (:maze/type opts)))

(defmulti start-coord
  (fn [opts _cell-map]
    (:maze/type opts)))

(defmulti maze-cells dispatch)

(defmulti closed-maze dispatch)
