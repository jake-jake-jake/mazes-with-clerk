(ns mazes.user
  (:require [nextjournal.clerk :as clerk]))

(defn start-server
  "start with watcher and show filter function to enable notebook pinning"
  []
  (clerk/serve! {:watch-paths ["notebooks" "src"]}))
(comment
  (start-server))

(defn build-static-site [opts]
  (clerk/build! opts))

(comment
  (clerk/build! {:paths ["notebooks/*"] :index "notebooks/index.md" :out-path "public/"}))
