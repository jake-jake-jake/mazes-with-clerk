(ns mazes.display
  (:require [mazes.algorithms :as algos]
            [mazes.graphics :as graphics]
            [mazes.graphics.multi :as multi]))

;; # Display Utility Functions
; Utility functions for displaying mazes.
(defn display-maze-with-circle-label-path [opts cells start-coord]
  (let [steps (algos/walk-maze-with-djikstra start-coord cells)
        display-opts (assoc opts
                            :maze/longest-path (apply max (vals steps))
                            :maze/xform " ")]
    (graphics/render-maze display-opts
                          cells
                          (map (fn [[coord n]]
                                 (graphics/circle-label coord n display-opts))
                               steps))))

(defn display-maze-with-flood-path [opts cells start-coord]
  (let [steps (algos/walk-maze-with-djikstra start-coord cells)
        display-opts (assoc opts
                            :maze/longest-path (apply max (vals steps)))]
    (graphics/render-maze display-opts
                          cells
                          (map (fn [[coord n]]
                                 (multi/flood-cell display-opts coord n))
                               steps))))

(defn display-maze-with-longest-flood-path [opts cells]
  (let [start-coord (rand-nth (algos/find-longest-path cells))
        steps (algos/walk-maze-with-djikstra start-coord cells)
        display-opts (assoc opts
                            :maze/longest-path (apply max (vals steps)))]
    (graphics/render-maze display-opts
                           cells
                           (map (fn [[coord n]]
                                  (multi/flood-cell display-opts coord n))
                                steps))))
