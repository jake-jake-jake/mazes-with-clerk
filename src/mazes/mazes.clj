(ns mazes.mazes
  "Mazes!")

(defn exec
  "Invoke me with clojure -X mazes.mazes/exec"
  [opts]
  (println "exec with" opts))

(defn -main
  "Invoke me with clojure -M -m mazes.mazes"
  [& args]
  (println "-main with" args))
