(ns mazes.algorithms
  (:require [mazes.algorithms.multi :as multi]
            [mazes.util :as util]
            [mazes.algorithms.load]))

(def default-opts
  {:maze/width     4
   :maze/height    4
   :maze/cell-size 40
   :maze/type      :grid})

(defn add-util-fns [{:keys [maze/width maze/height] :as opts}]
  (assoc opts
         :maze/eastmost?  (fn [x] (= x (dec width)))
         :maze/northmost? (fn [y] (= y (dec height)))))

(defn cells-by-row [{:keys [maze/width
                            maze/height]}]
  (for [y (range 0 height)
        x (range 0 width)]
    [x y]))

(defn cells-by-col [opts]
  (->> (cells-by-row opts)
       (sort-by (juxt first second))))

(defn closed-grid
  "Produce a fully closed grid for given maze options."
  [ctx]
  (multi/closed-maze ctx))

(defn neighbors [[x y]]
  [[(dec x) y] [(inc x) y]
   [x (dec y)] [x (inc y)]])

(defn cells->cell-map-impl [cells]
  (let [f (first cells)]
    (if (some? (:x f))
      (->> cells
           (map (fn [m] [[(:x m) (:y m)]
                         m]))
           (into {}))
      (->> cells
           (map (fn [m] [(:coord m) m]))
           (into {})))))

(def cells->cell-map (memoize cells->cell-map-impl))

(defn accessible-neighbors [coord cells]
  (when-let [c (get cells coord)]
    (let [[w e s n] (neighbors coord)]
      (cond-> []
        (and (not (:cell/north-wall? c))
             (get cells n))
        (conj n)

        (and (not (:cell/east-wall? c))
             (get cells e))
        (conj e)

        (when-let [s-c (get cells s)]
          (not (:cell/north-wall? s-c)))
        (conj s)

        (when-let [w-c (get cells w)]
          (not (:cell/east-wall? w-c)))
        (conj w)))))

(defn unvisited-neighbors [opts coord cells unv]
  (->> (multi/neighbors opts coord)
       (filter #(contains? cells %))
       (filter #(contains? unv %))))

(defn link-cells* [start end cell-map]
  (let [start-cell (get cell-map start)
        end-cell (get cell-map end)
        [x-move y-move] (mapv - end start)
        new-start-cell (cond-> start-cell
                         (pos? y-move) (assoc :cell/north-wall? false)
                         (pos? x-move) (assoc :cell/east-wall? false))
        new-end-cell (cond-> end-cell
                       (neg? y-move) (assoc :cell/north-wall? false)
                       (neg? x-move) (assoc :cell/east-wall? false))]
    (assoc cell-map
           start new-start-cell
           end   new-end-cell)))

(defn link-cells [opts s e m]
  (multi/link-cells opts s e m))

(defn closed-maze [opts]
  (multi/closed-maze opts))

;; maze algorithms
(defn binary-tree-maze [opts]
  (let [{:keys [:maze/northmost?
                :maze/eastmost?] :as opts} (add-util-fns opts)
        walls-fn (fn [[x y]]
                   (cond (northmost? y) :cell/north-wall?
                         (eastmost? x)  :cell/east-wall?
                         :else (util/srnd-nth [:cell/north-wall?
                                               :cell/east-wall?])))]
    (->> (cells-by-row opts)
         (mapv (fn [[x y]] {:x x :y y (walls-fn [x y]) true})))))

(defn sidewind
  ([{:keys [wall-fn wind-wall remove-wall base-cell]} cells]
   (loop [[[x y] & r] cells
          run     []
          out     nil]
     (if (some? x)
       (let [wall (wall-fn [x y])
             wind? (= wall wind-wall)
             run (conj run (assoc base-cell :x x :y y wind-wall wind?))]
         (if wind?
           (recur r [] (concat out (assoc-in run [(util/srnd-int (count run)) remove-wall] false)))
           (recur r run out)))
       (concat out run)))))

(defn sidewinder-maze [{:keys [:maze/sidewind-axis]
                        :or   {sidewind-axis :x}
                        :as   opts}]
  (let [{:keys [:maze/northmost?
                :maze/eastmost?] :as opts} (add-util-fns opts)
        wall-fn (fn [[x y]]
                  (cond (northmost? y)  :cell/north-wall?
                        (eastmost? x)   :cell/east-wall?
                        :else (util/srnd-nth [:cell/north-wall?
                                              :cell/east-wall?])))
        cells (case sidewind-axis
                :x (cells-by-row opts)
                :y (cells-by-col opts))
        part-fn (case sidewind-axis :x second :y first)
        wind-opts (case sidewind-axis
                    :x {:wall-fn wall-fn
                        :wind-wall :cell/east-wall?
                        :remove-wall :cell/north-wall?
                        :base-cell {:cell/north-wall? true}}
                    :y {:wall-fn wall-fn
                        :wind-wall :cell/north-wall?
                        :remove-wall :cell/east-wall?
                        :base-cell {:cell/east-wall? true}})]
    (->> cells
         (partition-by part-fn)
         (mapcat #(sidewind wind-opts %)))))

(defn aldous-broder-walk [opts start cell-map]
  (let [unvisited (into #{} (keys cell-map))]
    (loop [unvisited unvisited
           cells cell-map
           coord start]
      (let [unv (disj unvisited coord)
            neighbors (->> (multi/neighbors opts coord)
                           (filter #(contains? cells %)))
            next-cell (util/srnd-nth neighbors)
            should-update? (contains? unvisited next-cell)]
        (cond (not (seq unvisited))
              cells
              should-update?
              (recur unv (multi/link-cells opts coord next-cell cells) next-cell)
              :else (recur unv cells next-cell))))))

(defn aldous-broder-maze
  "Create a maze using the Aldous-Broder algorithm: choose a point randomly in the
  maze, and then proceed to step through it. The first time you enter a cell,
  link it from the cell you entered from. "
  [opts]
  (let [cell-map (multi/closed-maze opts)
        start-coord (multi/start-coord opts cell-map)]
    (vals (aldous-broder-walk opts start-coord cell-map))))

(defn wilson-walk [opts target cell-map]
  (let [unvisited (-> (into #{} (keys cell-map))
                      (disj target))
        start (util/srnd-nth (seq unvisited))]
    (loop [unvisited unvisited
           visited #{target}
           cells cell-map
           path []
           coord start]
      (let [should-update? (contains? visited coord)
            path-index (.indexOf path coord)
            crosses-path?  (not (neg? path-index))
            neighbors (when coord
                        (->> (multi/neighbors opts coord)
                             (filter #(contains? cells %))))
            next-cell (util/srnd-nth neighbors)
            next-path (conj path coord)]
        (cond (not (seq unvisited))
              cells
              ;; If we've crossed back into the path, remove the loop
              crosses-path?
              (recur unvisited visited cells (subvec path 0 path-index) coord)
              ;; If we entered a visited/target cell, update data and restart
              ;; the process from new unvisited cell
              should-update?
              (let [next-unvisited (reduce #(disj %1 %2) unvisited next-path)]
                (recur next-unvisited
                       (reduce #(conj %1 %2) visited next-path)
                       (reduce (fn [a [s e]]
                                 (multi/link-cells opts s e a)) cells (partition 2 1 next-path))
                       []
                       (-> next-unvisited seq util/srnd-nth)))
              :else  ;; otherwise, extend path and recur
              (recur unvisited visited cells (conj path coord) next-cell))))))

(defn wilson-maze
  "Create a maze using Wilson's algorithm: choose a random cell to visit, then
  choose a random unvisited cell, and draw a path from the unvisited cell to the
  target cell, erasing loops along the way. "
  ([opts]
   (let [cell-map (multi/closed-maze opts)
         coord (multi/start-coord opts cell-map)]
     (wilson-maze opts coord)))
  ([opts coord & cell-map]
   (let [cell-map (or cell-map
                      (multi/closed-maze opts))]
     (vals (wilson-walk opts coord cell-map)))))

(defn- hunt-next [opts unvisited cells]
  (let [[c visiteds] (->> unvisited
                          (sort-by (juxt first second))
                          (map (fn [c] [c (->> (multi/neighbors opts c)
                                               (filter #(contains? cells %))
                                               (remove #(contains? unvisited %)))]))
                          (remove (fn [[c ns]] (not (seq ns))))
                          first)]
    [(util/srnd-nth visiteds) c]))

(defn hunt-and-kill [opts coord cell-map]
  (let [unvisited (-> (into #{} (keys cell-map))
                      (disj coord))]
    (loop [unvisited unvisited
           cells cell-map
           coord coord]
      (let [prospects (unvisited-neighbors opts coord cells unvisited)
            dead-end? (not (seq prospects))]
        (cond (not (seq unvisited))  ;; if no more unvisited, return maze
              cells

              (not dead-end?)        ;; if not a dead-end, link the cells
              (let [n (util/srnd-nth prospects)]
                (recur (disj unvisited n) (multi/link-cells opts coord n cells) n))

              :else (let [[coord n] (hunt-next opts unvisited cells)]
                      (recur (disj unvisited n) (multi/link-cells opts coord n cells) n)))))))

(defn hunt-and-kill-maze
  ([opts]
   (let [closed-maze (multi/closed-maze opts)
         coord (multi/start-coord opts closed-maze)]
     (hunt-and-kill-maze opts coord)))
  ([opts coord]
   (let [cell-map (multi/closed-maze opts)]
     (vals (hunt-and-kill opts coord cell-map)))))

(defn recursive-backtrack [opts coord cell-map]
  (let [unvisited (-> (into #{} (keys cell-map))
                      (disj coord))]
    (loop [unvisited unvisited
           cells cell-map
           path [coord]]
      (let [l (peek path)
            prospects (unvisited-neighbors opts l cells unvisited)]
        (cond (not (seq unvisited))
              cells

              (seq prospects)
              (let [n (util/srnd-nth prospects)]
                (recur (disj unvisited n) (multi/link-cells opts l n cells) (conj path n)))

              :else (recur unvisited cells (pop path)))))))

(defn recursive-backtracker-maze
  ([opts]
   (let [closed-maze (multi/closed-maze opts)
         coord (multi/start-coord opts closed-maze)]
     (recursive-backtracker-maze opts coord)))
  ([opts coord]
   (let [cell-map (multi/closed-maze opts)]
     (vals (recursive-backtrack opts coord cell-map)))))

;; pathfinding functions
(defn- djikstra-step-maze [step to-visit visited maze]
  (if (seq to-visit)
    (let [new-visited (into visited
                            (map (fn [c] [c step]) to-visit))
          next-steps (->> (mapcat #(accessible-neighbors % maze) to-visit)
                          (remove #(contains? visited %))
                          dedupe)]
      (recur (inc step) next-steps new-visited maze))
    visited))

(defn walk-maze-with-djikstra [coord maze-cells]
  (let [cell-map (cells->cell-map maze-cells)]
    (djikstra-step-maze 1 (accessible-neighbors coord cell-map) {coord 0} cell-map)))

(defn furthest-point
  "Returns _one_ of the furthest points from given coordinate (if multiple) or the
  furthest point if one."
  [coord maze-cells]
  (->> (walk-maze-with-djikstra coord maze-cells)
       (sort-by second)
       reverse
       ffirst))

(defn find-longest-path [maze-cells]
  (let [cell-map (->> (map (fn [m] [(:coord m)
                                   (select-keys m [:cell/north-wall?
                                                   :cell/east-wall?])]) maze-cells)
                      (into {}))
        from-origin (djikstra-step-maze 1 (accessible-neighbors [0 0] cell-map) {[0 0] 0} cell-map)
        furthest-from-origin (->> from-origin
                                  (sort-by second)
                                  reverse
                                  ffirst)
        furthest-from-furthest (->> (djikstra-step-maze 1
                                                        (accessible-neighbors furthest-from-origin cell-map)
                                                        {furthest-from-origin 0}
                                                        cell-map)
                                    (sort-by second)
                                    reverse
                                    ffirst)]
    [furthest-from-origin furthest-from-furthest]))

(defn dead-end? [coord cells]
  (= 1
     (count (accessible-neighbors coord cells))))

(defn count-dead-ends [cells]
  (->> (keys cells)
       (map #(dead-end? % cells))
       (filter #(true? %))
       count))

(comment
  (def polar-opts {:maze/type :polar
                   :maze/cell-size 10
                   :maze/height 4
                   :maze/width 4})
  (hunt-and-kill-maze polar-opts))
