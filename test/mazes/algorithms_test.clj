(ns mazes.algorithms-test
  (:require [clojure.test :refer :all]
            [mazes.algorithms :as algos]
            [mazes.util :as util]))

(def test-seed 31)
(deftest test-add-util-fns
  (let [opts {:maze/width 4 :maze/height 4}
        updated-opts (algos/add-util-fns opts)]
    (is (= true ((:maze/eastmost? updated-opts) 3)))
    (is (= false ((:maze/eastmost? updated-opts) 2)))
    (is (= true ((:maze/northmost? updated-opts) 3)))
    (is (= false ((:maze/northmost? updated-opts) 2)))))

(deftest test-cells-by-row
  (let [opts {:maze/width 2 :maze/height 2}]
    (is (= [[0 0] [1 0] [0 1] [1 1]] (algos/cells-by-row opts)))))

(deftest test-cells-by-col
  (let [opts {:maze/width 2 :maze/height 2}]
    (is (= [[0 0] [0 1] [1 0] [1 1]] (algos/cells-by-col opts)))))

(deftest test-closed-grid
  (let [opts {:maze/width 2 :maze/height 2 :maze/type :grid}]
    (is (= [{:coord [0 0] :cell/north-wall? true :cell/east-wall? true}
            {:coord [1 0] :cell/north-wall? true :cell/east-wall? true}
            {:coord [0 1] :cell/north-wall? true :cell/east-wall? true}
            {:coord [1 1] :cell/north-wall? true :cell/east-wall? true}]
           (vals (algos/closed-grid opts))))))

(deftest test-neighbors
  (is (= [[-1 0] [1 0] [0 -1] [0 1]] (algos/neighbors [0 0]))))

(deftest test-cells->cell-map
  (let [cells [{:x 0 :y 0 :cell/north-wall? true :cell/east-wall? true}
               {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}]]
    (is (= {[0 0] {:x 0 :y 0 :cell/north-wall? true :cell/east-wall? true}
            [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}}
           (algos/cells->cell-map cells)))))

(deftest test-accessible-neighbors
  (let [cells {[0 0] {:x 0 :y 0 :cell/north-wall? false :cell/east-wall? true}
               [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}
               [0 1] {:x 0 :y 1 :cell/north-wall? true :cell/east-wall? true}}]
    (is (= [[0 1]] (algos/accessible-neighbors [0 0] cells)))))

(deftest test-unvisited-neighbors
  (let [cells {[0 0] {:x 0 :y 0 :cell/north-wall? true :cell/east-wall? true}
               [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}
               [0 1] {:x 0 :y 1 :cell/north-wall? true :cell/east-wall? true}}
        unv #{[1 0] [0 1]}]
    (is (= [[1 0] [0 1]] (algos/unvisited-neighbors [0 0] cells unv)))))

(deftest test-link-cells
  (let [cells {[0 0] {:x 0 :y 0 :cell/north-wall? true :cell/east-wall? true}
               [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}}]
    (is (= {[0 0] {:x 0 :y 0 :cell/north-wall? true :cell/east-wall? false}
            [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}}
           (algos/link-cells* [0 0] [1 0] cells)))))

;;; all of the following need to be improved once we can seed the generation of
;;; the maze
(deftest test-binary-tree-maze
  (let [opts {:maze/width 2 :maze/height 2}
        maze (util/with-seeded-random test-seed (algos/binary-tree-maze opts))]
    (is (= [{:x 0, :y 0, :cell/east-wall? true}
            {:x 1, :y 0, :cell/east-wall? true}
            {:x 0, :y 1, :cell/north-wall? true}
            {:x 1, :y 1, :cell/north-wall? true}]
           maze))))

(deftest test-sidewinder-maze
  (let [opts {:maze/width 2 :maze/height 2}
        maze (algos/sidewinder-maze opts)]
    (is (= 4 (count maze)))))

(deftest test-aldous-broder-maze
  (testing "grid mazes"
    (let [opts {:maze/width 3 :maze/height 3 :maze/type :grid}
          maze (util/with-seeded-random test-seed (algos/aldous-broder-maze opts))]
      (is (= [{:coord [0 0], :cell/north-wall? false, :cell/east-wall? true}
              {:coord [0 1], :cell/north-wall? false, :cell/east-wall? false}
              {:coord [0 2], :cell/north-wall? true, :cell/east-wall? true}
              {:coord [1 0], :cell/north-wall? true, :cell/east-wall? false}
              {:coord [1 1], :cell/north-wall? false, :cell/east-wall? false}
              {:coord [1 2], :cell/north-wall? true, :cell/east-wall? true}
              {:coord [2 0], :cell/north-wall? false, :cell/east-wall? true}
              {:coord [2 1], :cell/north-wall? false, :cell/east-wall? true}
              {:coord [2 2], :cell/north-wall? true, :cell/east-wall? true}]
             (sort-by :coord maze)))))
  (testing "polar mazes"
    (let [opts {:maze/cell-size 10 :maze/height 4 :maze/type :polar}
          maze (util/with-seeded-random test-seed (algos/aldous-broder-maze opts))]
      (is (= [{:coord [0 0],
               :ccw nil,
               :cw nil,
               :children [[1 0] [1 120] [1 240]],
               :parent nil,
               :closed-children #{[1 0] [1 240]},
               :cw-closed? true}
              {:coord [1 0],
               :ccw [1 240],
               :cw [1 120],
               :children nil,
               :parent [0 0],
               :closed-children #{},
               :cw-closed? false}
              {:coord [1 120],
               :ccw [1 0],
               :cw [1 240],
               :children nil,
               :parent [0 0],
               :closed-children #{},
               :cw-closed? false}
              {:coord [1 240],
               :ccw [1 120],
               :cw [1 0],
               :children nil,
               :parent [0 0],
               :closed-children #{},
               :cw-closed? true}]
             (sort-by :coord maze))))))

(deftest test-wilson-maze
  (let [opts {:maze/width 2 :maze/height 2 :maze/type :grid}
        maze (util/with-seeded-random test-seed (algos/wilson-maze opts))]
    (is (= [{:coord [0 0], :cell/north-wall? false, :cell/east-wall? true}
            {:coord [1 0], :cell/north-wall? false, :cell/east-wall? true}
            {:coord [0 1], :cell/north-wall? true, :cell/east-wall? false}
            {:coord [1 1], :cell/north-wall? true, :cell/east-wall? true}]
              maze))))

(deftest test-hunt-and-kill-maze
  (let [opts {:maze/width 2 :maze/height 2 :maze/type :grid}
        maze (algos/hunt-and-kill-maze opts)]
    (is (= 4 (count maze)))))

(deftest test-recursive-backtracker-maze
  (let [opts {:maze/width 2 :maze/height 2 :maze/type :grid}
        maze (algos/recursive-backtracker-maze opts)]
    (is (= 4 (count maze)))))

(deftest test-walk-maze-with-djikstra
  (let [maze (algos/aldous-broder-maze {:maze/width 2
                                        :maze/height 2
                                        :maze/type :grid})
        distances (algos/walk-maze-with-djikstra [0 0] maze)]
    (is (contains? distances [0 0]))))

(deftest test-furthest-point
  (let [maze (algos/aldous-broder-maze {:maze/width 2 :maze/height 2 :maze/type :grid})
        point (algos/furthest-point [0 0] maze)]
    (is (vector? point))))

(deftest test-find-longest-path
  (let [maze (algos/aldous-broder-maze {:maze/width 2 :maze/height 2 :maze/type :grid})
        path (algos/find-longest-path maze)]
    (is (= 2 (count path)))))

(deftest test-dead-end?
  (let [cells {[0 0] {:x 0 :y 0 :cell/north-wall? true :cell/east-wall? false}
               [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}}]
    (is (true? (algos/dead-end? [1 0] cells)))
    (is (true? (algos/dead-end? [0 0] cells)))))

(deftest test-count-dead-ends
  (let [cells {[0 0] {:x 0 :y 0 :cell/north-wall? true :cell/east-wall? false}
               [1 0] {:x 1 :y 0 :cell/north-wall? true :cell/east-wall? true}}]
    (is (= 2 (algos/count-dead-ends cells)))))
