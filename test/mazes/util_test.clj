(ns mazes.util-test
  (:require [mazes.util :as util]
            [clojure.test :refer :all]))

(deftest with-seeded-random-test
  (let [c (into [] (range 1000))]
    (doseq [seed (range 100)]
      (is (= (util/with-seeded-random seed (util/srnd-nth c))
             (util/with-seeded-random seed (util/srnd-nth c)))))))
