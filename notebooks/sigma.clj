(ns sigma
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.algorithms :as algos]
            [mazes.algorithms.multi :as algos.m]
            [mazes.display :as display]
            [mazes.graphics :as graphics]))


{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def opts {:maze/type :sigma
           :maze/cell-size 20
           :maze/height 8
           :maze/width 20})

;;; # Sigma Mazes
;; Mazes that have hexagonal cells are called "sigma" mazes. We should be able
;; to use most of the implementations we have built to generate them.

;; We want to generate a square base for the maze, which means we will have
;; a "stack" of cells that is the width we provide wide, and the height we
;; provide high, staggered because that is the way that hexagons fit together.


;; We can produce the list of axial coordinates for a given rectangular sigma
;; maze in this way:

{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn sigma-coords
  [{:keys [maze/width maze/height]}]
  (mapcat identity (for [r (range height)
                         :let [n (quot r 2)
                               qs (take width (range (- n) (+ (- n) width)))]]
                     (map (fn [q] [q r]) qs))))

;; Here are the axial coordinates for a maze of height 4 and width 10:

{:nextjournal.clerk/visibility {:code :hide :result :show}}
(def coords (algos.m/maze-cells opts))

{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def closed (algos/closed-maze opts))

;; Here is this as a closed maze:
{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze opts (vals closed)))


;; Linking the [0 0] cell to the [1 0] looks like this:
{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def linked (algos/link-cells opts [0 0] [1 0] closed))
{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze opts (vals linked)))

;; The existing implementations we have for the more robust algorithms should
;; seamlessly produce sigma mazes.

;; Here is a Recursive Backtracker maze:

{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def backtracker (algos/recursive-backtracker-maze opts))
{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze opts backtracker))

;; And here is a Hunt and Kill maze:

{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def hk (algos/hunt-and-kill-maze opts))
{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze opts hk))
