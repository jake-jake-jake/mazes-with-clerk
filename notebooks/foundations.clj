^{:nextjournal.clerk/visibility {:code :hide}}
(ns foundations
  (:require [nextjournal.clerk :as clerk]))

;;; # Laying foundations
;; These are the elemental components of our mazes.

;;; ## The canvas
;; Mazes will be drawn in stages: first, we will put in place the full footprint
;; of our maze. Because our algorithms rely on the edge cells always being
;; closed, we need to draw a closed box. We'll give it a stroke width that is
;; more legible than 1px.

;; Assuming a 25 pixel cell width, if we wanted a 4x3 base for a maze, we would
;; want something that looks like this:
(clerk/html
 [:svg {:width "100" :height "75"  :xmlns "http://www.w3.org/2000/svg"}
  [:rect {:width "75" :height "75" :stroke "black" :fill "white" :stroke-width 7}]])

;; It would be nice to be able to declare the canvas for our maze, like so:
^{:nextjournal.clerk/visibility {:result :hide}}
(def example-canvas {:cell-size 25
                     :height 3
                     :width 4})

^{:nextjournal.clerk/visibility {:result :hide}}
(defn maze-base [{:keys [cell-size height width]} & contents]
  (let [pix-h (* cell-size height)
        pix-w (* cell-size width)]
    [:svg {:width pix-w :height pix-h :xmlns "http://www.w3.org/2000/svg"}
     [:rect {:width pix-w :height pix-h :stroke "black" :fill "white" :stroke-width 7}]
     contents]))

;; We can then use our function to declare arbitrary maze foundations:
(clerk/html (maze-base {:cell-size 25 :height 4 :width 3}))

(clerk/html (maze-base {:cell-size 25 :height 4 :width 8}))

(clerk/html (maze-base {:cell-size 40 :height 3 :width 10}))

;;; ## Cell walls
;; The early algorithms in _Mazes_ only draw two walls per cell, the top and the
;; right, or the north and the east wall. The bottom and the left are considered
;; to be set by either the edges of the maze, or by a neighboring cell.

;; A function for producing cell walls might look like this:
^{:nextjournal.clerk/visibility {:result :hide}}
(defn maze-cell [{:keys [x
                         y
                         cell-size
                         top?
                         right?]}]
  (let [x1 (or x 0)
        x2 (+ x1 cell-size)
        y1 (or y 0)
        y2 (+ y1 cell-size)]
    [:svg
     (when top?
       [:line {:x1 x1 :x2 x2 :y1 y1 :y2 y1 :stroke "black" :stroke-width 3}])
     (when right?
       [:line {:x1 x2 :x2 x2 :y1 y1 :y2 y2 :stroke "black" :stroke-width 3}])]))

;; This will produce the interior walls of a maze:
^{:nextjournal.clerk/visibility {:code :hide}}
(clerk/html
 [:svg {:width "100" :height "100" :stroke-width "7"  :xmlns "http://www.w3.org/2000/svg"}
  (maze-cell {:x 0 :y 0 :cell-size 25 :top? true :right? true})])

;;; ## Combining base and interior
;; You might expect, if your brain works like mine, that a circle in `0` y
;; position would be at the bottom, like so:
^{:nextjournal.clerk/visibility {:code :hide}}
(clerk/html
 (maze-base {:cell-size 40 :height 3 :width 10}
            [:circle {:cx 0 :cy 120 :r 15 :fill "red"}]))

;; That is not the case: the y position is relative to the **top** of the SVG
;; image, not the bottom.
(clerk/html
 (maze-base {:cell-size 40 :height 3 :width 10}
            [:circle {:cx 0 :cy 0 :r 15 :fill "red"}]))

;; We will handle this by stepping backward from the most distant y value in
;; steps of cell size. Thankfully, declaring this in clojure is easy.
(let [{:keys [cell-size
              width
              height]}    {:cell-size 25
                           :width     4
                           :height    4}]
  (for [y (reverse (range 0
                          (inc (* cell-size height))
                          cell-size))
        x (range 0 (* cell-size width) cell-size)]
    [x y]))

;; We can factor this into a function, which we will call cells-by-row.
^{:nextjournal.clerk/visibility {:result :hide}}
(defn cells-by-row [{:keys [cell-size
                            width
                            height]}]
  (for [y (reverse (range 0
                          (inc (* cell-size height))
                          cell-size))
        x (range 0 (* cell-size width) cell-size)]
    [x y]))

;; We can now combine the maze base with the cell walls function to draw a fully
;; filled in grid.
^{:nextjournal.clerk/visibility {:result :hide}}
(defn draw-maze [{:keys [cell-size] :as opts}]
  (maze-base opts
             (for [[x y] (cells-by-row opts)]
               (maze-cell {:x x :y y :cell-size cell-size :top? true :right? true}))))

(clerk/html
 (draw-maze {:cell-size 40 :height 5 :width 18}))

;; With these foundations we can implement the binary tree and sidewinder maze
;; generation algorithms.
