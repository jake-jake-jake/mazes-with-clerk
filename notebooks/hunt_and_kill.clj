(ns hunt-and-kill
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.algorithms :as algos]
            [mazes.display :as display]
            [mazes.graphics :as graphics]))
;;; # Constraining Random Walks
;; Random walks completely eliminate bias from mazes. This is (ostensibly) a
;; good thing. An issue with it, though, is that the algorithms that do it (e.g.
;; Aldous-Broder or Wilson) meander aimlessly over the potential space of the
;; maze, and do not keep track of cells they have visited when choosing their
;; next step. This means we can spin our wheels for quite some time when making
;; the grids.

;; We can make this more efficient by _constraining_ potential next steps. Of
;;course, this does insert a bit of bias into the maze, but so it goes.

;;; ## Hunt-and-Kill Algorithm
;; The Hunt and Kill algorithm adds a constraint to random walks: instead of
;; meandering _anywhere_, we only select the next cell we visit from unvisited
;; cells. If we come to a dead end, we move through unvisited cells until we
;; find one that is adjacent to visited cells, then we link that to the visited
;; cell and proceed with our walk.

;; This tends to produce mazes that have a lot of what Jamis Buck calls _river_:
;; they have long paths.

;; Here are a few example mazes.
^{:nextjournal.clerk/visibility {:result :hide}}
(def large-maze-opts {:maze/width 36
                      :maze/height 36
                      :maze/cell-size 20
                      :maze/type :grid})

^{:nextjournal.clerk/visibility {:result :hide}}
(def hk1 (algos/hunt-and-kill-maze large-maze-opts))

^{:nextjournal.clerk/visibility {:result :hide}}
(def hk2 (algos/hunt-and-kill-maze large-maze-opts))

^{:nextjournal.clerk/visibility {:result :hide}}
(def hk3 (algos/hunt-and-kill-maze large-maze-opts))

(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts hk1))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts hk2))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts hk3))


;;; ## Recursive Backtracker Mazes
;; Another algorithm that constrains random walks is the recursive backtracker.
;; It keeps track of the path that has been taken through the maze and moves
;; back through the maze until it finds another place it could have turned to an
;; unvisited cell.


^{:nextjournal.clerk/visibility {:result :hide}}
(def rb1 (algos/recursive-backtracker-maze large-maze-opts))

^{:nextjournal.clerk/visibility {:result :hide}}
(def rb2 (algos/recursive-backtracker-maze large-maze-opts))

^{:nextjournal.clerk/visibility {:result :hide}}
(def rb3 (algos/recursive-backtracker-maze large-maze-opts))

(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts rb1))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts rb2))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts rb3))
