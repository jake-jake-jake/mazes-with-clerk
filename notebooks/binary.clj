(ns binary
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.display :as display]
            [mazes.graphics :as graphics :refer [maze-base grid-cell]]
            [mazes.algorithms :as algo]
            [mazes.util :as util]))

;;; # Binary Tree Mazes
;; To produce this type of maze, we walk every cell and choose whether to leave
;; the top or the right side open.

;; We can use the `maze-base` and `maze-cell` functions from `mazes-graphics` to
;; implement a `draw-maze` function.

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn draw-maze [{:keys [cell-size] :as opts}]
  (maze-base opts
             (for [[x y] (graphics/cells-by-row opts)]
               (grid-cell {:x x :y y :cell-size cell-size :top? true :right? true}))))

;; As implemented above it produces a fully closed grid:
(clerk/html
 (draw-maze {:cell-size 40 :width 18 :height 10}))

;; We need a way to "flip a coin" in each cell, and only fill in either the top
;; or the right.

(clerk/code '(rand-nth [:top? :right?]))

;; We can implement this simply by tweaking the `draw-maze` function:
^{:nextjournal.clerk/visibility {:result :hide :code :show}}
(defn draw-binary-tree-maze [{:keys [cell-size] :as opts}]
  (maze-base opts
             (for [[x y] (graphics/cells-by-row opts)
                   :let [filled-side (util/srnd-nth [:top? :right?])]]
               (grid-cell {:x x :y y :cell-size cell-size filled-side true}))))

^{:nextjournal.clerk/visibility {:result :hide}}
(defn draw-binary-tree-maze-1 [{:keys [cell-size] :as opts}]
  (maze-base opts
             (for [[x y] (graphics/cells-by-row opts)
                   :let [filled-side (util/srnd-nth [:top? :right?])]]
               (grid-cell {:x x :y y :cell-size cell-size filled-side true}))))
(clerk/html
 (draw-binary-tree-maze-1 {:cell-size 40 :width 18 :height 10}))

;; While this initially looks like it produces a maze, the implementation has a
;; bug. The rightmost and the topmost cells may be completely closed off. _Mazes
;; for Programmers_ produces "perfect" mazes, which means that every cell is
;; connected to each other cell by exactly one path. If we do not coerce the
;; rightmost cells to always be open on the top, and the topmost cells to always
;; being open on the right, there may be cells that we cannot reach.

;; We need to refactor the choice function so that it always selects to leave
;; the top open on the right side, and the right open on the top.

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
'(cond (is-topmost? y)   :top?
       (is-rightmost? x) :right?
       :else (util/srnd-nth [:top? :right?]))

;; These functions can be added to the definition of draw-maze.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn draw-binary-tree-maze [{:keys [cell-size
                                     width] :as opts}]
  (let [is-rightmost? (fn [x] (= x (* cell-size (dec width))))
        is-topmost?   (fn [y] (= y 0))]
    (maze-base opts
               (for [[x y] (graphics/cells-by-row opts)
                     :let [filled (cond (is-topmost? y)        :top?
                                        (is-rightmost? x)      :right?
                                        :else (util/srnd-nth [:top? :right?]))]]
                 (grid-cell {:x x :y y :cell-size cell-size filled true})))))
(clerk/html
 (draw-binary-tree-maze {:cell-size 40 :width 18 :height 10}))

;; Keeping track of just one cell is fine for a simple algorithm like this, but
;; for more complicated algorithms we will want to be able to keep track of the
;; state of more than one cell. To do that we will need to adjust how we flow
;; through them.

;;; ## Another Look
;; Let's consider a square sized binary maze with 20 pixel cells, 36 by 36 cells
;; in size. If we use color and opacity to represent how many steps it takes to
;; reach a point in the maze from one end of the longest path, this is what we
;; see.

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def square-binary-tree-opts {:maze/cell-size 20
                              :maze/height 36
                              :maze/width 36
                              :maze/type :grid})
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def square-maze-cells (algo/binary-tree-maze square-binary-tree-opts))
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def furthest-point (algo/furthest-point [0 0] square-maze-cells))

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(clerk/html (display/display-maze-with-flood-path square-binary-tree-opts
                                                  square-maze-cells
                                                  furthest-point))

;; The texture of the maze is clear here: we progress all the way to the top,
;; then head back down to the origin row. If we start from the center of the
;; maze, we see the bias clearly, as an accessible diagonal swath of the maze.

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(clerk/html (display/display-maze-with-flood-path square-binary-tree-opts
                                                  square-maze-cells
                                                  [17 18]))

;; Starting from the northeasternmost cell, we see a fairly even gradient
;; through to the origin.

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(clerk/html (display/display-maze-with-flood-path square-binary-tree-opts
                                                  square-maze-cells
                                                  [35 35]))

;; From the northwest corner, we see a stepwise gradation in distance.

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(clerk/html (display/display-maze-with-flood-path square-binary-tree-opts
                                                  square-maze-cells
                                                  [0 35]))

;; And from the southeast corner, we see similar, but with less breadth across
;; the maze.

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(clerk/html (display/display-maze-with-flood-path square-binary-tree-opts
                                                  square-maze-cells
                                                  [35 0]))

;; There probably is a way to codify "smoothness" of gradation evident when we
;; proceed from the point that orients the maze's bias.
