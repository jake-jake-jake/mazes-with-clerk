(ns sidewinder
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.graphics :as graphics :refer [maze-base grid-cell cells-by-row]]
            [mazes.algorithms :as algos]
            [mazes.algorithms :as algo]))

;;; # Sidewinder Mazes
;; The sidewinder algorithm adds in rudimentary backtracking across cells in a
;; row. While moving across each row, we maintain a run of cells when we leave
;; the right walls open. When we randomly determine that the right side should
;; be closed, we do not automatically leave the top open. Instead, we randomly
;; choose a random cell from the run and place the path forward there. This
;; produces mazes with more varied texture than binary tree mazes such as the
;; one below.

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(defn draw-binary-tree-maze [{:keys [cell-size
                                     width] :as opts}]
  (let [is-rightmost? (fn [x] (= x (* cell-size (dec width))))
        is-topmost?   (fn [y] (= y 0))]
    (maze-base opts
               (for [[x y] (cells-by-row opts)
                     :let [filled (cond (is-topmost? y)        :top?
                                        (is-rightmost? x)      :right?
                                        :else (rand-nth [:top? :right?]))]]
                 (grid-cell {:x x :y y :cell-size cell-size filled true})))))
^{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html
 (draw-binary-tree-maze {:cell-size 40 :width 18 :height 10}))

;; Our initial implementation doesn't have an easy way to pass through state
;; from one cell to the next. It also combines creating and rendering the maze
;; into a single step. We'll adjust things so that we can reduce across a
;; sequence of ordered cells, then feed the resulting sequence of cells to a
;; render function.

;; First we need a function that will sidewind each row of cells.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn sidewind-run
  ([cells path-fn]
   (sidewind-run cells path-fn [] nil))
  ([[f & r] path-fn run out]
   (if (seq f)
     (let [closed-wall (path-fn f)
           c {:x (first f)
              :y (second f)
              :top? :true
              :right? (= closed-wall :right?)}
           run (conj run c)]
       (if (= closed-wall :right?)
         (recur r path-fn [] (concat out (assoc-in run [(rand-int (count run)) :top?] false)))
         (recur r path-fn run out)))
     out)))

;; And we need a renderer that will produce an SVG from a list of cells.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn render-maze [opts cells]
  (maze-base opts
             (map (fn [c] (grid-cell (assoc c :cell-size (:cell-size opts)))) cells)))

;; We can put these together and have a generic sidewinder maze function.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn draw-sidewinder-maze [{:keys [cell-size
                                    width] :as opts}]
  (let [is-rightmost? (fn [x] (= x (* cell-size (dec width))))
        is-topmost?   (fn [y] (= y 0))
        path-fn (fn [[x y]] (cond (is-topmost? y)         :top?
                                  (is-rightmost? x)      :right?
                                  :else (rand-nth [:top? :right?])))
        cells (->> (cells-by-row opts)
                   (partition-by second)
                   (mapcat #(sidewind-run % path-fn)))]
    (render-maze opts cells)))

^{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (draw-sidewinder-maze {:cell-size 40 :width 18 :height 8}))

;; Sidewinder mazes display more irregularity than binary tree mazes, but they
;; still have some weaknesses, including that they are far easier to traverse in
;; the direction they were assembled, and that the top side are always runs the
;; length of the maze.

;;; ## Another Look
;; Using the refactored generation and rendering functions from [A Second Look
;; at Foundations](./notebooks/foundations2/index.html) we can do a bit more
;; with sidewinder mazes. The implementation above wound around the x-axis, but
;; we can generalize the winding function, so that we can wind around y.

;; I noodled a bit to come up with this. It is not wholly satisfactory (it
;; requires passing in a function for selecting which wall is closed, which wall
;; we wind on, which wall we remove, and the base cell). It seems like we could
;; refactor it so that it takes an vector of weights for the path, but that
;; would entail another refactor of the foundational model we're working with.
;; So this will do for now:

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn sidewind
  ([{:keys [wall-fn wind-wall remove-wall base-cell]} cells]
   (loop [[[x y] & r] cells
          run     []
          out     nil]
     (if (some? x)
       (let [wall (wall-fn [x y])
             wind? (= wall wind-wall)
             run (conj run (assoc base-cell :x x :y y wind-wall wind?))]
         (if wind?
           (recur r [] (concat out (assoc-in run [(rand-int (count run)) remove-wall] false)))
           (recur r run out)))
       (concat out run)))))

;; This can be called by a maze generation function, with the option to wind
;; around different axes:
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
'(defn sidewinder-maze [{:keys [:maze/sidewind-axis]
                        :or   {sidewind-axis :x}
                        :as   opts}]
  (let [{:keys [:maze/northmost?
                :maze/eastmost?] :as opts} (add-util-fns opts)
        wall-fn (fn [[x y]]
                  (cond (northmost? y)  :cell/north-wall?
                        (eastmost? x)   :cell/east-wall?
                        :else (rand-nth [:cell/north-wall?
                                         :cell/east-wall?])))
        cells (case sidewind-axis
                :x (cells-by-row opts)
                :y (cells-by-col opts))
        part-fn (case sidewind-axis :x second :y first)
        wind-opts (case sidewind-axis
                    :x {:wall-fn wall-fn
                        :wind-wall :cell/east-wall?
                        :remove-wall :cell/north-wall?
                        :base-cell {:cell/north-wall? true}}
                    :y {:wall-fn wall-fn
                        :wind-wall :cell/north-wall?
                        :remove-wall :cell/east-wall?
                        :base-cell {:cell/east-wall? true}})]
    (->> cells
         (partition-by part-fn)
         (mapcat #(sidewind wind-opts %)))))
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def maze-opts {:maze/cell-size 40
                :maze/height    8
                :maze/width     18
                :maze/type      :grid})

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def x-winder-maze (algos/sidewinder-maze maze-opts))

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def y-winder-maze (algos/sidewinder-maze (assoc maze-opts
                                                 :maze/sidewind-by :y)))

;; This is a maze wound around the x-axis:
(clerk/html (graphics/render-maze maze-opts x-winder-maze))

;; This is a maze wound around the y-axis:
(clerk/html (graphics/render-maze maze-opts y-winder-maze))

;; We can plot the paths through them with Djikstra's algorithm using this
;; function:
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn display-maze-with-path [opts cells start-coord]
  (let [steps (algos/walk-maze-with-djikstra start-coord cells)
        display-opts (assoc opts
                            :maze/longest-path (apply max (vals steps))
                            :maze/xform " ")]
    (graphics/render-maze display-opts
                          cells
                          (map (fn [[coord n]]
                                 (graphics/circle-label coord n display-opts))
                               steps))))

(clerk/html (display-maze-with-path maze-opts x-winder-maze [0 0]))

(clerk/html (display-maze-with-path maze-opts y-winder-maze [0 0]))
