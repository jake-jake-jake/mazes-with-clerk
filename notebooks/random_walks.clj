(ns random-walks
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.algorithms :as algos]
            [mazes.display :as display]
            [mazes.graphics :as graphics]))

;;; # Creating Mazes with Random Walks
;; The Binary Tree and Sidewinder algorithms produce mazes with strong biases;
;; this results from them requiring certain paths at certain locations in the
;; maze. We can avoid this by doing truly random walks: from any cell, we can
;; potentially move into any adjacent cell.

;; There are many ways of doing this. We'll start with the first introduced in
;; _Mazes for Programmers:_ the Aldous-Broder algorithm.
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def small-maze-opts {:maze/height 10
                      :maze/width 18
                      :maze/cell-size 40
                      :maze/type :grid})

;;; ## Aldous-Broder Mazes
;; Aldos-Broder begin with a fully closed maze.
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def closed-grid (vals (algos/closed-grid small-maze-opts)))

;; The first step is to choose a random cell from the maze, and then randomly
;; move into one of its neighbors. If this is the first time that cell has been
;; visited, link the two cells. If not, do not link them. Proceed to randomly
;; walk the maze until all cells have been visited.
(clerk/html (graphics/render-maze small-maze-opts closed-grid))

;; Given our grid, we can randomly choose a starting cell, and a random neighbor:
(def start (-> closed-grid
               rand-nth
               :coord))

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def example-cell-map (algos/cells->cell-map closed-grid))

(def end (->> (algos/neighbors start)
              (filter #(contains? example-cell-map %))
              rand-nth))

;; After we link the two cells, our grid looks like this:
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def updated-cell-map (algos/link-cells* start end example-cell-map))

(clerk/html (graphics/render-maze small-maze-opts (vals updated-cell-map)))

;; To finish the maze we continue until all cells have been visited, and we end
;; up with something like this:
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def aldous-broder-cells (algos/aldous-broder-maze small-maze-opts))

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def longest-path (algos/find-longest-path aldous-broder-cells))

(clerk/html (display/display-maze-with-flood-path small-maze-opts
                                                  aldous-broder-cells
                                                  (first longest-path)))

;; The drawback with this algo is it does not account for prioritizing movement
;; to unvisited cells. It simply randomly walks the grid until _all_ cells have
;; been visited. For larger mazes, this can be time consuming. Or, in the words
;; of _Mazes for Programmers_, slow to finish.

;;; ## Wilson Mazes
;; We can modify the walking algorithm a bit by not merely walking over visited
;; cells. Instead, we choose one cell to be "visited" and then another to be our
;; starting point. We randomly walk from the starting point, removing any loops
;; in the path we're tracing, until we enter a visited cell. At that point, we
;; update the cells with the new path, and randomly choose a new unvisited cell
;; as our starting point, restarting the process until there are no unvisited
;; cells.

;; This is a more or less straightforward recursive operation.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
'(defn wilson-walk [target cell-map]
  (let [unvisited (-> (into #{} (keys cell-map))
                      (disj target))
        start (rand-nth (seq unvisited))]
    (loop [unvisited unvisited
           visited #{target}
           cells cell-map
           path []
           coord start]
      (let [should-update? (contains? visited coord)
            path-index (.indexOf path coord)
            crosses-path?  (not (neg? path-index))
            neighbors (when coord
                        (->> (neighbors coord)
                             (filter #(contains? cells %))))
            next-cell (rand-nth neighbors)
            next-path (conj path coord)]
        (cond (not (seq unvisited))
              cells
              ;; If we've crossed back into the path, remove the loop
              crosses-path?
              (recur unvisited visited cells (subvec path 0 path-index) coord)
              ;; If we entered a visited/target cell, update data and restart
              ;; the process from new unvisited cell
              should-update?
              (let [next-unvisited (reduce #(disj %1 %2) unvisited next-path)]
                (recur next-unvisited
                       (reduce #(conj %1 %2) visited next-path)
                       (reduce (fn [a [s e]]
                                 (link-cells s e a)) cells (partition 2 1 next-path))
                       []
                       (-> next-unvisited seq rand-nth)))
              :else  ;; otherwise, extend path and recur
              (recur unvisited visited cells (conj path coord) next-cell))))))

^{:nextjournal.clerk/visibility {:result :hide}}
(def wilson-cells (algos/wilson-maze small-maze-opts))

;; This produces mazes similar to those created by Aldous-Broder, but
;; these "finish faster:" we do not meander over previously visited cells near
;; the termination of the algorithm, waiting until we finally stumble into the
;; last one.
^{:nextjournal.clerk/visibility {:result :hide}}
(def longest-path-wilson (algos/find-longest-path wilson-cells))
(clerk/html (display/display-maze-with-flood-path small-maze-opts
                                                  wilson-cells
                                                  (first longest-path-wilson)))

;; Here are a few example mazes.
^{:nextjournal.clerk/visibility {:result :hide}}
(def large-maze-opts {:maze/width 36
                      :maze/height 36
                      :maze/cell-size 20
                      :maze/type :grid})

^{:nextjournal.clerk/visibility {:result :hide}}
(def abm1 (algos/aldous-broder-maze large-maze-opts))
^{:nextjournal.clerk/visibility {:result :hide}}
(def abm2 (algos/aldous-broder-maze large-maze-opts))
^{:nextjournal.clerk/visibility {:result :hide}}
(def wm1 (algos/wilson-maze large-maze-opts))
^{:nextjournal.clerk/visibility {:result :hide}}
(def wm2 (algos/wilson-maze large-maze-opts))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts abm1))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts abm2))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts wm1))
(clerk/html (display/display-maze-with-longest-flood-path large-maze-opts wm2))
