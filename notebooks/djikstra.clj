(ns djikstra
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.algorithms :as algos]
            [mazes.display :refer [display-maze-with-circle-label-path]]
            [mazes.graphics :as graphics :refer [render-maze]]
            [mazes.algorithms :as algo]))

;;; # Walking Mazes with Djikstra's Algorithm
;; We need some way of automatically solving mazes, in order to prove they are
;; solvable and to gauge how difficult they are. Consider a maze with these
;; settings.

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def sample-maze {:maze/cell-size 40
                  :maze/height    8
                  :maze/width     18
                  :maze/type      :grid})

;; We can produce a binary tree maze for that height easily.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def binary-tree-cells (algos/binary-tree-maze sample-maze))

;; Rendering it produces this maze.
(clerk/html (render-maze sample-maze binary-tree-cells))

;; Since we're not using an object to keep track of links between cells, we need
;; a way to see which neighbors are accessible from a given cell.

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn accessible-neighbors [coord cells]
  (when-let [c (get cells coord)]
    (let [[w e s n] (algos/neighbors coord)]
      (cond-> []
        (and (not (:cell/north-wall? c))
             (get cells n))
        (conj n)

        (and (not (:cell/east-wall? c))
             (get cells e))
        (conj e)

        (when-let [s-c (get cells s)]
          (not (:cell/north-wall? s-c)))
        (conj s)

        (when-let [w-c (get cells w)]
          (not (:cell/east-wall? w-c)))
        (conj w)))))

;; Using this we can recursively walk a maze.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn- djikstra-step-maze [step to-visit visited maze]
  (if (seq to-visit)
    (let [new-visited (into visited
                            (map (fn [c] [c step]) to-visit))
          next-steps (->> (mapcat #(accessible-neighbors % maze) to-visit)
                          (remove #(contains? visited %))
                          dedupe)]
      (recur (inc step) next-steps new-visited maze))
    visited))

^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(defn walk-maze-with-djikstra [coord maze-cells]
  (let [cell-map (->> (map (fn [m] [[(:x m) (:y m)]
                                    (select-keys m [:cell/north-wall?
                                                    :cell/east-wall?])]) maze-cells)
                      (into {}))]
    (djikstra-step-maze 1 (accessible-neighbors coord cell-map) {coord 0} cell-map)))

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(def djikstra-walk (walk-maze-with-djikstra [9 4] binary-tree-cells))

;; We can use this result to add a layer of information onto our rendering of
;; the maze. Because I am having trouble rotating the text after I mirror it on
;; the x-axis, this projection of the maze treats positive y values as
;; descending.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def djikstra-opts (assoc sample-maze
                          :maze/xform " "
                          :maze/longest-path (apply max (vals djikstra-walk))))

^{:nextjournal.clerk/visibility {:code :show :result :show}}
(clerk/html (render-maze djikstra-opts
                         binary-tree-cells
                         (map (fn [[coord n]]
                                (graphics/circle-label coord n djikstra-opts))
                              djikstra-walk)))
;; And we can find the longest path (or one of the longest paths) by running
;; through the maze twice from the origin.

(clerk/html (display-maze-with-circle-label-path djikstra-opts
                                    binary-tree-cells
                                    (algo/furthest-point [7 7] binary-tree-cells)))

;; If we make the maze larger, for these simple algorithms, the origin row tends
;; to hold one end of the longest path, and oftentimes both ends of the maze.
^{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def square-opts (assoc djikstra-opts
                        :maze/height 18
                        :maze/width 18))

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def square-cells (algo/binary-tree-maze square-opts))
(clerk/html (display-maze-with-circle-label-path square-opts
                                    square-cells
                                    (algo/furthest-point [7 7] square-cells)))

^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def wind-cells (algo/sidewinder-maze square-opts))
(clerk/html (display-maze-with-circle-label-path square-opts
                                    wind-cells
                                    (algo/furthest-point [7 7] wind-cells)))
