^{:nextjournal.clerk/visibility {:code :hide}}
(ns foundations2
  (:require [mazes.algorithms :as algo]
            [mazes.graphics :as graphics]
            [nextjournal.clerk :as clerk]))
;;; # A Second Look at Foundations
;; The method for drawing mazes I first [outlined](../foundations/index.html)
;; tangles representing each cell with creating it. This is fine for simply
;; producing mazes but if we want to analyze or extend them, or modify the way
;; they are rendered, it would be preferable to separate the description of the
;; maze from rendering it.

;; The first thing we need to address is the `cells-by-row` function. It tangles
;; the coordinates of the cell with how we have to transform them into x and y
;; coordinates specific to SVG. This confuses things when you try to use path
;; algorithms to walk the maze (the algorithm cares about the description of the
;; cell and its neighbors, not how the cell is displayed).

;; A descriptive `cells-by-row` function would simply produce coordinates of
;; cells by row, according to the options used to produce the maze:

^{:nextjournal.clerk/visibility {:result :hide}}
(def maze-opts
  {:maze/width     18
   :maze/height    9
   :maze/cell-size 40
   :maze/type      :grid})
^{:nextjournal.clerk/visibility {:code :show}}
(def maze-coords (algo/cells-by-row maze-opts))

;; Such a list of coordinates could be used to build up a descriptive sequence
;; of the walls inside of a maze. Notice we change `top?` and `right?` to the
;; more precise `north-wall?` and `east-wall?`:
^{:nextjournal.clerk/visibility {:code :hide}}
(def bin-maze (algo/binary-tree-maze maze-opts))

;; And we could then, with reference to the options we used to produce the maze,
;; render it:
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def maze-svg (graphics/render-maze maze-opts bin-maze))
^{:nextjournal.clerk/visibility {:code :hide}}
(clerk/html maze-svg)

;; Doing this requires that we calculate the pixel position of the walls of each
;; cell in the cell rendering phase, not the cell sequence phase:

^{:nextjournal.clerk/visibility {:result :hide}}
(defn render-cell [{:keys [x y :cell/north-wall? :cell/east-wall?]}
                   {:keys [maze/cell-size maze/height]}]
  (let [x1 (* x cell-size)
        x2 (+ x1 cell-size)
        y1 (* (- height y) cell-size)
        y2 (- y1 cell-size)]
    [:svg
     (when north-wall?
       [:line {:x1 x1 :x2 x2 :y1 y1 :y2 y1 :stroke "black" :stroke-width 3 :stroke-linecap "round"}])
     (when east-wall?
       [:line {:x1 x2 :x2 x2 :y1 y1 :y2 y2 :stroke "black" :stroke-width 3 :stroke-linecap "round"}])]))

;; By doing it this way, we leave the `x` and `y` coordinates in the maze
;; description clear. We don't have to translate them from pixel position back
;; to maze coordinates.

;; There is a slight issue, however: as is, this rendering produces mazes that
;; do not quite match expectation. They are mirrored on the x-axis. The y-axis
;; proceeds from 0 _at the top_ toward the bottom.
^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def unflipped-maze
  (graphics/render-maze (assoc maze-opts
                               :maze/xform "") bin-maze))
^{:nextjournal.clerk/visibility {:code :hide}}
(clerk/html (conj unflipped-maze [:circle {:cx 20 :cy 21 :r 13 :fill "red"}] ))
;; To handle this, we need to instruct our maze base to transform the elements
;; of the maze, by grouping all maze components in a `g` element and then
;; scaling and translating them (pay attention to the `xform`):

^{:nextjournal.clerk/visibility {:result :hide}}
(defn render-base [{:keys [maze/cell-size maze/height maze/width maze/xform]} & contents]
  (let [pix-h (* cell-size height)
        pix-w (* cell-size width)
        xform (or xform
                  (format  "scale(1,-1) translate(0, %s)" (- pix-h)))]  ;; default to producing a maze as expected
    [:svg {:width pix-w :height pix-h :xmlns "http://www.w3.org/2000/svg"}
     [:g {:transform xform}
      [:rect {:width pix-w :height pix-h :stroke "black" :fill "white" :stroke-width 7}]
      contents]]))

;; This produces the binary-tree maze we expect, with the bias toward the upper
;; right, as if we had incrementally moved through the cells beginning in the
;; bottom left.
^{:nextjournal.clerk/visibility {:code :hide}}
(clerk/html maze-svg)

;; We can use these functions to enrich our display of mazes, including with
;; information on path distance.
