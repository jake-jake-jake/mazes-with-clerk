``` clojure
(ns mazes.index
  {:nextjournal.clerk/visibility {:result :show :code :hide}}
  (:require [babashka.fs :as fs]
            [mazes.util :refer [symbol->link]]
            [nextjournal.clerk :as clerk]
            [clojure.string :as str]))
```
# Notebooks on Mazes with Programmers
I am reading through [_Mazes for
Programmers_](http://www.mazesforprogrammers.com/), by Jamis Buck. These
notebooks demonstrate implementations of the algorithms in the book along with
some remarks from me.

1. [Foundations](./notebooks/foundations/index.html) 
2. [Binary Tree](./notebooks/binary/index.html)
3. [Sidewinder](./notebooks/sidewinder/index.html)
4. [A Second Look at Foundations](./notebooks/foundations2/index.html) 
5. [Pathfinding with Djikstra](./notebooks/djikstra/index.html) 
6. [Avoiding Bias with Random Walks](./notebooks/random_walks/index.html) 
7. [Constraining Random Walks](./notebooks/hunt_and_kill/index.html) 
8. [Polar Mazes](./notebooks/polar/index.html) 
9. [Sigma Mazes](./notebooks/sigma/index.html) 

## Made with...
I'm using [Clerk](https://github.com/nextjournal/clerk) to generate the
notebooks. Gitlab CI builds a static site and serves it. The code is
[public](https://gitlab.com/jake-jake-jake/mazes-with-clerk).

