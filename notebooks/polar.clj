(ns polar
  {:nextjournal.clerk/visibility {:code :hide}}
  (:require [nextjournal.clerk :as clerk]
            [mazes.algorithms :as algos]
            [mazes.algorithms.multi :as algos.m]
            [mazes.display :as display]
            [mazes.graphics :as graphics]))

;;; # Polar Mazes
;; Polar mazes are drawn inside a circle, with each cell representing a section
;; of a ring.

;; The implementation we are using here index cells with a 2-element vector. The
;; first indicates the ring the cell is a part of, and the second the angle of
;; rotation for the first point on the cells outer arc.

;; Here is a set of options.

{:nextjournal.clerk/visibility {:code :show :result :hide}}
(def maze-opts {:maze/type :polar
                :maze/cell-size 20
                :maze/height 15})

;; We can produce a closed polar grid by using our newly implemented multi
;; method:

{:nextjournal.clerk/visibility {:code :hide :result :show}}
(def closed (algos.m/closed-maze maze-opts))

;; A closed grid for a polar maze resembles an increasingly subdivided pie
;; chart.

(clerk/html (graphics/render-maze maze-opts (vals closed) nil))

;; Intuitively, we might believe that the random walking algorithms would work
;; off the shelf with polar mazes. (I know I did, when I was refactoring them to
;; try them out.) Here is a polar maze produced by the Aldous-Broder algorithm.
(def aldous-broder-polar-maze (sort-by :coord (algos/aldous-broder-maze maze-opts)))

(clerk/html (graphics/render-maze maze-opts aldous-broder-polar-maze nil))

;; We can see that Aldous-Broder does not work very well; the decision to update
;; the maze depends on keeping track of unvisited cells and only linking mazes
;; when we enter an unvisited cell. Since we can end up walking back into
;; visited cells and walking through them to visited cells that are not linked
;; to them, we can leave portions of the maze isolated.

{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def wilson-maze (sort-by :coord (algos/wilson-maze maze-opts)))

{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze maze-opts wilson-maze))

;; Here is a Hunt and Kill maze.

{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def r-b-maze (sort-by :coord (algos/hunt-and-kill-maze maze-opts)))

{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze maze-opts r-b-maze))

;; And finally, here is a Recursive Backtracker maze.

{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(def r-b-maze (sort-by :coord (algos/recursive-backtracker-maze maze-opts)))

{:nextjournal.clerk/visibility {:code :hide :result :show}}
(clerk/html (graphics/render-maze maze-opts r-b-maze))
